# -*- coding: utf-8 -*-
"""
This is a skeleton file that can serve as a starting point for a Python
console script. To run this script uncomment the following lines in the
[options.entry_points] section in setup.cfg:

    console_scripts =
         fibonacci = carranza_xyz.skeleton:run

Then run `python setup.py install` which will install the command `fibonacci`
inside your current environment.
Besides console scripts, the header (i.e. until _logger...) of this file can
also be used as template for Python modules.

Note: This skeleton file can be safely removed if not needed!
"""

import argparse
import dns.resolver
import sys
import logging

from carranza_xyz import __version__

__author__ = "Brie Carranza"
__copyright__ = "Brie Carranza"
__license__ = "mit"

_logger = logging.getLogger(__name__)


def count_ns_records(domain):
    """A function that counts the number of NS records for the specified domain

    This function will initiate a DNS resolver and then query for the NS record(s) for the domain name
    passed to the function. The number of NS records will be returned. If the DNS resolver can not be
    initiated, an exception will occur. If there are no NS records identified, ValueError will occur.
    """
    ns_records = list()
    try:
        myResolver = dns.resolver.Resolver()
    except:                        # pragma: no cover
        print("Sorry. I could not create a new instance of resolver.Resolver().")   # pragma: no cover
    try:
        dns_answers = myResolver.query(domain, "NS") 
    except:
        print("Oops. It's DNS. It's always DNS.")
        raise ValueError("No NS records found.")
    else:
        print(dns_answers)
        for rr in dns_answers:
            ns_records.append(str(rr))
    return len(ns_records)

def find_dev_record(domain):
    print("We are looking for dev.{} in DNS. Will we find it?".format(domain))
    sought_record = "dev." + domain
    dev_record = str()
    try:
        dev_resolver = dns.resolver.Resolver()
        my_resolver.nameservers = ['8.8.8.8']
    except:                        # pragma: no cover
        print("Sorry. I could not create a new instance of resolver.Resolver().")   # pragma: no cover
    try:
        dns_answers = dev_resolver.resolve(sought_record, "CNAME")
    except:
        print("Oops. It's DNS. It's always DNS.")
        raise ValueError("No CNAME record found.")
    else:
        for rr in dns_answers:
            dev_record = str(rr)
    return dev_record


def parse_args(args): #pragma: no cover
    """Parse command line parameters

    Args:
      args ([str]): command line parameters as list of strings

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(
        description="Just a Fibonacci demonstration")
    parser.add_argument(
        "--version",
        action="version",
        version="carranza_xyz {ver}".format(ver=__version__))
    parser.add_argument(
        dest="n",
        help="The root of the domain you want to query",
        type=str,
        metavar="INT")
    parser.add_argument(
        "-v",
        "--verbose",
        dest="loglevel",
        help="set loglevel to INFO",
        action="store_const",
        const=logging.INFO)
    parser.add_argument(
        "-vv",
        "--very-verbose",
        dest="loglevel",
        help="set loglevel to DEBUG",
        action="store_const",
        const=logging.DEBUG)
    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(level=loglevel, stream=sys.stdout,
                        format=logformat, datefmt="%Y-%m-%d %H:%M:%S")


def main(args): # pragma: no cover
    """Main entry point allowing external calls

    Args:
      args ([str]): command line parameter list
    """
    args = parse_args(args)
    setup_logging(args.loglevel)
    _logger.debug("Starting crazy calculations...")
    print("There are  {} NS records for the specified domain.".format(count_ns_records
  (args.n)))
    dev_record_value = find_dev_record(args.n)
    print(dev_record_value)
    _logger.info("Script ends here")


def run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])        #pragma: no cover


if __name__ == "__main__":
    run()
