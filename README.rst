============
carranza_xyz
============


Test that the DNS configuration for carranza.xyz is as I would like it. 


Description
===========

The important stuff is in the `tests` directory. That is where I define the desired DNS configuration. If the tests pass, all is well. If the tests pass, there is a problem with the DNS configuration for the domain. (There could also be some transient problem with completing the query.)


Note
====

This project has been set up using PyScaffold 3.2.3. For details and usage
information on PyScaffold see https://pyscaffold.org/.
