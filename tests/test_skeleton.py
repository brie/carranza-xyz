# -*- coding: utf-8 -*-

import pytest
from carranza_xyz.skeleton import count_ns_records
from carranza_xyz.skeleton import setup_logging
from carranza_xyz.skeleton import find_dev_record

__author__ = "Brie Carranza"
__copyright__ = "Brie Carranza"
__license__ = "mit"


def test_count_ns_records():
    assert count_ns_records("carranza.xyz") == 4
    assert count_ns_records("root-servers.net") == 13
    assert count_ns_records("example.com") == 2
    with pytest.raises(ValueError):
        count_ns_records(".com")

def test_setup_logging():
    assert setup_logging("INFO") == None
    assert setup_logging("DEBUG") == None

def test_find_dev_record():
    """
    Make sure that you can find dev.carranza.xyz -> brie.gitlab.io
    """
    assert "brie.gitlab.io." in find_dev_record("carranza.xyz")
    with pytest.raises(ValueError):
        find_dev_record("never.carranza.xyz")