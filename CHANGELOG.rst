=========
Changelog
=========

Version 0.1
===========

- Initial implementation!
- Feature: Count NS records
- Feature: Query for dev. CNAME record
